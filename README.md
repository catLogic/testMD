[![Build status](https://gitlab.com/gitlab-org/gitlab-ee/badges/master/build.svg)](https://gitlab.com/gitlab-org/gitlab-ee/commits/master)
[![CE coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=coverage)](https://gitlab-org.gitlab.io/gitlab-ce/coverage-ruby)
[![Code Climate](https://codeclimate.com/github/gitlabhq/gitlabhq.svg)](https://codeclimate.com/github/gitlabhq/gitlabhq)
[![Core Infrastructure Initiative Best Practices](https://bestpractices.coreinfrastructure.org/projects/42/badge)](https://bestpractices.coreinfrastructure.org/projects/42)

# MarkDown

> ### gitlab的markdown语法 [看这里](https://gitlab.com/help/user/markdown.md)


<!--## TOC-->

<!-->>>-->

<!--### 目录-->

<!--[TOC]-->

<!-->>>-->

## 标题

# 一级标题
## 二级标题
### 三级标题

## 字体

~~这是删除线~~

**这是加粗**

__这也是加粗__ 

$`a^2+b^2=c^2`$

$`a + b = c`$

$`a / b = c`$


`变色突显`

这是添加的[+additions+]添加添加[+additions+]哎哎哎

这是删除的[-deletions-]删除删除[-deletions-]啊啊啊

这是添加的{+additions添加添加additions+}哎哎哎

这是删除的[-deletions删除删除deletions-]啊啊啊

@123 某人或组织

@all 整个组织

## 脚注
这是脚注：[^sample_footnote]
[^sample_footnote]: 这是脚注信息

## 列表

### 无序列表
* 项目一
* 项目二
* 项目三
    * 项目三的子项目一
    * 项目三的子项目二
        * 项目三的子项目二的子项目一
            * 项目三的子项目二的子项目一的子项目一
        
### 有序列表
1. 项目一
2. 项目二
3. 项目三
    4. 项目三的子项目一
    5. 项目三的子项目二
        6. 项目三的子项目二的子项目一
            7. 项目三的子项目二的子项目一的子项目一     
8. 项目四


### 任务列表
- [ ] 任务一`未做任务`
- [x] 任务二`已做任务`

## 图片
直接贴图
![图片名字cat](img/pic_9.jpg)

HTML调整图片
<div align=center>
<img src="http://i1.piimg.com/4851/2715891fd18de750.jpg" width="150" height="200" alt="检察长"/>
</div>
 
## 链接

* 地址链接： https://www.baidu.com
* 标签链接：[百度](https://www.baidu.com)
* 邮箱链接：<yhtanga@isoftstone.com>
* 文档链接1：[test1](./test1.md)
* 文档链接2：[test2](doc/test2.md) 

## 区块引用

>>>
这是引用：六大原则
 >单一职责原则

 >里氏替换原则

 >依赖倒置原则

 >接口隔离原则

 >迪米特法则

 >开闭原则 

>>>

```
这是程序员最喜欢贴代码的区块

 viewModel.signupEnabled
            .subscribe(onNext: { [weak self] valid  in
                self?.signupOutlet.isEnabled = valid
                self?.signupOutlet.alpha = valid ? 1.0 : 0.5
            })
            .disposed(by: disposeBag)
```

## 分割线

***
---


## 图表
### 表格

| 第一列 | 第二列 | 第三列 |
| :-- | :-: | --: |
| 这是（1，1） | 这是（1，2） | 这是（1，3） |
| 这是（2，1） | 这是（2，2） | 这是（2，3） |






